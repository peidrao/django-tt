# django-tt

## Vídeo (resumo)

https://youtu.be/xN3EJruEtJE

## CASOS DE USO

[x] Cadastro de usuário

OBS: Não consegui salvar o nome e sobrenome, somente nome do usuário e sua senha.

[x] Autenticação (Login)

OBS: A autênticação usando Token só foi possível usando o postman

[x] Fazer um post

[x] Feed

OBS: O usuário não pode ver seus próprios tweets no feed, nem em seu "perfil"


## ENDPOINTS 

### Cadastro de usuários

**Rota:** http://127.0.0.1:8000/api/v1/users/

### Atualizar / Deletar

**Rota:** http://127.0.0.1:8000/api/v1/users/{id}

### Login

**Rota:** http://127.0.0.1:8000/auth/login/


### Token - via POSTMON

**Rota:** http://127.0.0.1:8000/api/v1/api-token-auth

![Print da requisição](./.media/screen.jpg)
 

### Logout

**Rota:** http://127.0.0.1:8000/auth/logout/

### Tweet (FEED)

**Rota:** http://127.0.0.1:8000/api/v1/tweets/


## BANCO DE DADOS   

Foi utilizado tanto o sqlite (que já vem como padrão, após criar migrations) como também o postgresql. Já existem dados no qslite. 


## INICIAR APLICAÇÃO

> pip install -r requirements.txt 

> python manage.py runserver

Caso for usar o postgresql (precisa comentar o DATABASE referente ao sqlite e descomentar o do postgresql)

> docker-compose up -d

