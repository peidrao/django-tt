from rest_framework import serializers
from django.contrib.auth.models import User
from users.models import UserProfile


class UserSerializer(serializers.HyperlinkedModelSerializer):
    tweets = serializers.HyperlinkedRelatedField(many=True, view_name='tweets-v1:tweet-detail', read_only=True)
    first_name = serializers.CharField(source='userprofile.first_name')
    last_name = serializers.CharField(source='userprofile.last_name')


    class Meta:
        model = User
        fields = ('url', 'id', 'username', 'password', 'tweets', 'first_name', 'last_name')
        extra_kwargs = {'password': {'write_only': True}}
        read_only_fields = ('is_staff', 'is_superuser',
                            'is_active', 'date_joined',)

    def create(self, validated_data):
        
        user = User.objects.create(username=validated_data['username'])
        user.set_password(validated_data['password'])
        user.save()

        profile_data = validated_data.pop('userprofile')
        user.userprofile.first_name = profile_data['first_name']
        user.userprofile.last_name = profile_data['last_name']

        return user

    def update(self, instance, validated_data):
        if 'password' in validated_data:
            password = validated_data.pop('password')
            instance.set_password(password)

        profile_data = validated_data.pop('userprofile')
        instance.userprofile.first_name = profile_data['first_name']
        instance.userprofile.last_name = profile_data['last_name']

        return super(UserSerializer, self).update(instance, validated_data)