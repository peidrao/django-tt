from django.urls import path, include
from users import views
from rest_framework.routers import DefaultRouter
from rest_framework.schemas import get_schema_view
from rest_framework.authtoken import views as rest_framework_views

app_name = 'users'

router = DefaultRouter()
router.register('users', views.UserViewSet)

schema_view = get_schema_view(title='Users API')

urlpatterns = [
    path('', include(router.urls)),
    path('schema/', schema_view),
    path('api-token-auth/', rest_framework_views.obtain_auth_token, name='get-auth-token'),
]