from users.serializers import UserSerializer
from django.contrib.auth.models import User
from rest_framework import permissions
from users.permissions import IsStaffOrTargetUser
from rest_framework import viewsets
from tweets.models import Tweet
from tweets.serializers import TweetSerializer
from rest_framework.response import Response
from rest_framework.decorators import action


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def get_permissions(self):
        if self.request.method == 'POST':
            self.permission_classes = [permissions.AllowAny, ]
        else:
            self.permission_classes = [
                permissions.IsAuthenticated, IsStaffOrTargetUser, ]

        return super(UserViewSet, self).get_permissions()


    @action(detail=True, methods=['post'])
    def tweets(self, request, pk=None):
        author = self.get_object()
        tweets = Tweet.objects.filter(author=author)
        context = {'request': request}
        tweet_serializer = TweetSerializer(tweets, many=True, context=context)
        return Response(tweet_serializer.data)    
    
    