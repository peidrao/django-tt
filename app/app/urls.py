
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1/', include('users.urls', namespace='users')),
    path('api/v1/', include('tweets.urls', namespace='tweets-v1')),
    path('auth/', include('rest_framework.urls', namespace='rest_framework')),
]
