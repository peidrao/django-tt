from django.db.models import Q
from django.core.exceptions import PermissionDenied
from django.contrib.auth import get_user_model

from rest_framework import permissions
from rest_framework import viewsets
from rest_framework import generics

from tweets.models import Tweet
from users.models import UserProfile
from tweets.serializers import TweetSerializer
from tweets.permissions import IsCreationOrRead

User = get_user_model()


class TweetViewSet(viewsets.ModelViewSet):
    queryset = Tweet.objects.all()
    serializer_class = TweetSerializer
    permission_classes = [IsCreationOrRead, ]

    def get_queryset(self):
        #queryset = Tweet.objects.filter(author=queryset_user)
        queryset = Tweet.objects.exclude(author=self.request.user)
        return queryset.order_by('-created_at')
   

    def perform_create(self, serializer):
        serializer.save(author=self.request.user) 

  
