def convert_time(dt, dt_now):
	time = dt_now - dt

	if time.days > 0 :
		return dt.strftime('%b %d')
	elif time.seconds // 3600 > 0 :
		return str(time.seconds // 3600) + 'h'
	elif time.seconds // 60 > 0 :
		return str(time.seconds // 60) + 'm'
	else :
		return 'Now'