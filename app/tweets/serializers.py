from rest_framework import serializers
from tweets.models import Tweet
from datetime import datetime, timezone

from tweets.utils import convert_time


class TweetSerializer(serializers.HyperlinkedModelSerializer):
    author = serializers.ReadOnlyField(source='author.username')
    url = serializers.HyperlinkedIdentityField(view_name='tweets-v1:tweet-detail')

    class Meta:
        model = Tweet
        fields = ('url', 'author', 'text', 'created_at')
    
    def to_representation(self, instance):
        represetation = super(TweetSerializer, self).to_representation(instance)
        request = self.context['request']
        user = not request.user
        represetation['created_at'] = convert_time(instance.created_at, datetime.now(timezone.utc))
        return represetation