from django.urls import path, include
from tweets import views
from rest_framework.routers import DefaultRouter
from rest_framework.schemas import get_schema_view
from rest_framework.authtoken import views as rest_framework_views

app_name = 'tweets'

# Create a router and register our viewsets with it.
router = DefaultRouter()
router.register('tweets', views.TweetViewSet)

schema_view = get_schema_view(title='Tweets API')

urlpatterns = [
    path('', include(router.urls)),
    path('schema/', schema_view),
]
