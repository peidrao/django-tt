from django.db import models



class Tweet(models.Model):
    text = models.CharField(max_length=250)
    author = models.ForeignKey('auth.User', related_name='tweets', on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('created_at',)